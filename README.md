# Kali NetHunter Boot Animation

The original boot animation (1920x1080, 25fps) could be too small for your bigger screen phone since released. This script does what can be done with hand, which is a very simple process though.

## Demo

- [src](./src/src.mp4)
- [src_glitch](./src_glitch/src_glitch.mp4)
- [src_kali](./src_kali/src_kali.mp4)
- [src_mk](./src_mk/src_mk.mp4)
- _[src_ctos](./src_ctos/src_ctos.mp4)_ <!-- Not in ./config.sh -->

## To convert the original NetHunter boot animation

`./config.sh`

NOTE: You probably will not need to resize the pics, so try with answering `n` for first use.

Also keep in mind that this script will not change the ratio of the pictures (eg. for landscape tablet, etc) so it will be still the vertical logo.

## To install via NetHunter Terminal

`./install.sh`

## If you need a zip to install via TWRP

`./buildtwrp.sh`

## Compatibility

Its been working on the majority of devices.
